## Проекты блока С (C Projects)

- <a href="https://gitlab.com/school218063421/cprojects/school21">Бассейн / S21 Pool</a>
- <a href="https://gitlab.com/school218063421/cprojects/school21_simplebash">SimpleBash</a>
- <a href="https://gitlab.com/school218063421/cprojects/school21_stringplus">String+</a>
- <a href="https://gitlab.com/school218063421/cprojects/s21_decimal">Decimal</a>
- <a href="https://gitlab.com/school218063421/cprojects/school21_math">Math</a>
- <a href="https://gitlab.com/school218063421/cprojects/school21_matrix">Matrix</a>
- <a href="https://gitlab.com/school218063421/cprojects/school21_brickgame_v1.0">Brick Game 1.0 (Console Tetris)</a>

- <a href="https://gitlab.com/school218063421/cprojects">ВСЕ ПРОЕКТЫ БЛОКА</a>

## Проекты блока C++

- <a href="https://gitlab.com/school218063421/cpp_projects/matrix-plus-oop">Matrix +</a>

- <a href="https://gitlab.com/school218063421/cpp_projects">ВСЕ ПРОЕКТЫ БЛОКА</a>

## SQL Bootcamp

- <a href="https://gitlab.com/school218063421/sql-projects/s21-sql-bootcamp">Проекты блока SQL Bootcamp</a>

## DO-I (Linux + Docker + CI/CD)

- <a href="https://gitlab.com/school218063421/linux-administration/school21_linux_do1">Linux DO-I</a>
- <a href="https://gitlab.com/school218063421/linux-administration/school21_linux_do1_network">Linux Network</a>
- <a href="https://gitlab.com/school218063421/docker/school21_simpledocker">Docker</a>
- <a href="https://gitlab.com/school218063421/gitlab-ci-cd">Gitlab CI/CD</a>